package de.humanfork.experiment.spring.beandefinition.proxy;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.springframework.context.annotation.Import;

/**
 * Create instances for the two interfaces {@link ProxyServiceInterfaceA} and {@link ProxyServiceInterfaceB}.
 */
@Target({ElementType.TYPE, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Import(ProxyBeanRegistrar.class)
public @interface EnableProxies {

}
