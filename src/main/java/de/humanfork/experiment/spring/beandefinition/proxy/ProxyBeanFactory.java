//package de.humanfork.experiment.spring.beandefinition.proxy;
//
//import org.springframework.beans.factory.BeanClassLoaderAware;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Component;
//
//import de.humanfork.experiment.spring.beandefinition.BackendService;
//import lombok.Setter;
//
///**
// * This is a factory bean for type safe construction.
// * 
// * Enabled when {@link ProxyBeanFactory} use this bean declaration:
// * <pre><code>
// *  private &lt;T&gt; AbstractBeanDefinition proxyBean(final Class&lt;T&gt; implementingInterface) {
// *      return BeanDefinitionBuilder
// *              .rootBeanDefinition(implementingInterface)
// *              .setFactoryMethodOnBean("newProxyInstance", ProxyBeanFactory.PROXY_BEAN_FACTORY_NAME)
// *              .addConstructorArgValue(implementingInterface)
// *              .getBeanDefinition();
// *  }
// * </code></pre>
// */
//@Component(value = ProxyBeanFactory.PROXY_BEAN_FACTORY_NAME)
//public class ProxyBeanFactory implements BeanClassLoaderAware {
//    
//    public static final String PROXY_BEAN_FACTORY_NAME = "de.humanfork.experiment.spring.beandefinition.proxy.ProxyBeanFactory";
//
//    @Setter
//    private ClassLoader beanClassLoader;
//
//    @Autowired
//    private BackendService backendService;
//
//    public <T> T newProxyInstance(final Class<T> implementingInterface) {
//        return ProxyInvocationHandlerImplementation
//                .newProxyInstance(this.beanClassLoader, implementingInterface, this.backendService);
//    }
//}
