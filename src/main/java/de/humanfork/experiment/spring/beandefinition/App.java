package de.humanfork.experiment.spring.beandefinition;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import de.humanfork.experiment.spring.beandefinition.proxy.EnableProxies;

@SpringBootApplication
@EnableProxies
public class App {

    public static void main(final String[] args) {
        SpringApplication.run(App.class, args);
    }

}
