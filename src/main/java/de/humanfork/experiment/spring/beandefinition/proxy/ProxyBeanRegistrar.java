package de.humanfork.experiment.spring.beandefinition.proxy;

import java.util.List;

import org.springframework.beans.factory.BeanClassLoaderAware;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanNameGenerator;
import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
import org.springframework.core.type.AnnotationMetadata;

import lombok.Setter;

/**
 * This registrar create Spring Bean instances for the two interfaces {@link ProxyServiceInterfaceA} and {@link ProxyServiceInterfaceB}.
 * 
 * <p>
 * The important point is, that the two created Spring Beans are Java Dynamic Proxies.
 * This procies invoke a normal Spring Bean if they are invoked.
 * </p>
 * 
 * <p>
 * This class must not be a {@code @Configuration} class, instead it must been activated via {@code @Import}.
 * To outline this fact, use the meta Annotation {@link EnableProxies}.
 * </p>
 *
 */
public class ProxyBeanRegistrar implements ImportBeanDefinitionRegistrar, BeanClassLoaderAware {

    private List<Class<? extends Object>> proxyServiceInterfaces = List.of(ProxyServiceInterfaceA.class,
            ProxyServiceInterfaceB.class);

    @Setter
    private ClassLoader beanClassLoader;

    @Override
    public void registerBeanDefinitions(final AnnotationMetadata importingClassMetadata,
            final BeanDefinitionRegistry registry,
            final BeanNameGenerator importBeanNameGenerator) {

        for (Class<? extends Object> implementingInterface : this.proxyServiceInterfaces) {
            AbstractBeanDefinition beanDefinition = proxyBean(implementingInterface);

            /* The name is builded from a bean definition for the proxy interface, but not from the InvocationHandler bean,
             * because else all beans would be named: "ProxyInvocationHandlerImplementation" */
            String proxyBeanName = importBeanNameGenerator.generateBeanName(
                    BeanDefinitionBuilder.rootBeanDefinition(implementingInterface).getBeanDefinition(),
                    registry);
            System.out.println("**************************** register: " + proxyBeanName);
            registry.registerBeanDefinition(proxyBeanName, beanDefinition);
        }
        

    }

    private <T> AbstractBeanDefinition proxyBean(final Class<T> implementingInterface) {
        return BeanDefinitionBuilder
                .rootBeanDefinition(ProxyInvocationHandlerImplementation.class) //note that the Bean Definition class is the HandlerImplmentation!
                .addConstructorArgValue(this.beanClassLoader)
                .addConstructorArgValue(implementingInterface)
                .addConstructorArgReference("backendService")
                .setFactoryMethod("newProxyInstance")
                .getBeanDefinition();
    }
    
//    /**
//     * Alternative Factory with bean instead of static factory method 
//     * see: de.humanfork.experiment.spring.beandefinition.proxy.ProxyBeanFactory
//     */
//    private <T> AbstractBeanDefinition proxyBean(final Class<T> implementingInterface) {
//        return BeanDefinitionBuilder
//                .rootBeanDefinition(implementingInterface)
//                .setFactoryMethodOnBean("newProxyInstance", ProxyBeanFactory.PROXY_BEAN_FACTORY_NAME)
//                .addConstructorArgValue(implementingInterface)
//                .getBeanDefinition();
//    }

}
