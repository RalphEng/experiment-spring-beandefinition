package de.humanfork.experiment.spring.beandefinition;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import de.humanfork.experiment.spring.beandefinition.proxy.ProxyServiceInterfaceA;
import de.humanfork.experiment.spring.beandefinition.proxy.ProxyServiceInterfaceB;
import lombok.Getter;

@Service
@Getter
public class MyConsumingService {

    @Autowired
    private ProxyServiceInterfaceA proxyServiceInterfaceA;

    @Autowired
    private ProxyServiceInterfaceB proxyServiceInterfaceB;
}
