package de.humanfork.experiment.spring.beandefinition.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;

import de.humanfork.experiment.spring.beandefinition.BackendService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * {@link InvocationHandler} for Java Dynamic Proxy that <b>invoke a Spring bean</b> for every invoked method.
 * 
 * <p>{@link Object#hashCode()}, {@link Object#equals(Object)} and {@link Object#toString()} method invocations are not forwareded.</p>
 */
@RequiredArgsConstructor
@Getter
public class ProxyInvocationHandlerImplementation implements InvocationHandler {

    /** {@link Object#hashCode()} method. */
    private static final Method HASH_CODE_METHOD;

    /** {@link Object#equals(Object)} method. */
    private static final Method EQUALS_METHOD;

    /** {@link Object#toString()} method. */
    private static final Method TO_STRING_METHOD;
    
    static {
        try {
            HASH_CODE_METHOD = Object.class.getMethod("hashCode", (Class[]) null);
            EQUALS_METHOD = Object.class.getMethod("equals", new Class[] { Object.class });
            TO_STRING_METHOD = Object.class.getMethod("toString", (Class[]) null);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * The Spring bean that is invoked.
     */
    private final BackendService backendService;

    @Override
    public Object invoke(final Object proxy, final Method method, final Object[] args) throws Throwable {
        Class<?> declaringClass = method.getDeclaringClass();
        if (declaringClass == Object.class) {
            if (method.equals(HASH_CODE_METHOD)) {
                return proxyHashCode(proxy);
            } else if (method.equals(EQUALS_METHOD)) {
                return proxyEquals(proxy, args[0]);
            } else if (method.equals(TO_STRING_METHOD)) {
                return proxyToString(proxy);
            } else {
                throw new InternalError("unexpected Object method dispatched: " + method);
            }
        } else {
            System.out.println("invoke " + method + " : " + Arrays.toString(args));
            /* Do the proxy logic here */
            return this.backendService.getValue();
        }
    }

    protected int proxyHashCode(final Object proxy) {
        return System.identityHashCode(proxy);
    }

    protected boolean proxyEquals(final Object proxy, final Object other) {
        return proxy == other;
    }

    protected String proxyToString(final Object proxy) {
        return proxy.getClass().getName() + '@' + Integer.toHexString(proxy.hashCode());
    }

    /**
     * Factory method to create an new Proxy that forward the invocations to an new implementation instance.
     *
     * @param <T> the interface that is implemented by the proxy
     * @param classLoader the class loader
     * @param implementingInterfacethe interface that is implemented by the proxy
     * @param backendService the backend service
     * @return the proxy
     */
    public static <T> T newProxyInstance(final ClassLoader classLoader, final Class<T> implementingInterface,
            final BackendService backendService) {

        return implementingInterface.cast(Proxy.newProxyInstance(classLoader,
                new Class[] { implementingInterface },
                new ProxyInvocationHandlerImplementation(backendService)));
    }
}
