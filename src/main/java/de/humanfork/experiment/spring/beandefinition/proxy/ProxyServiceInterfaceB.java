package de.humanfork.experiment.spring.beandefinition.proxy;

public interface ProxyServiceInterfaceB {

    int doSomethingElse();
}
