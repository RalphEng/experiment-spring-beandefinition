package de.humanfork.experiment.spring.beandefinition.proxy;

public interface ProxyServiceInterfaceA {

    int doSomething();
}
