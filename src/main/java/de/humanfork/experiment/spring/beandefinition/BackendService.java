package de.humanfork.experiment.spring.beandefinition;

import org.springframework.stereotype.Service;

@Service
public class BackendService {

    public static final int RESULT = 42;

    public int getValue() {
        return RESULT;
    }
}
