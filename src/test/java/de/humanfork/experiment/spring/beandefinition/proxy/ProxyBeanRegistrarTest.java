package de.humanfork.experiment.spring.beandefinition.proxy;

import static org.assertj.core.api.Assertions.assertThat;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import de.humanfork.experiment.spring.beandefinition.BackendService;
import de.humanfork.experiment.spring.beandefinition.MyConsumingService;

@SpringBootTest
public class ProxyBeanRegistrarTest {

    @Autowired
    private ProxyServiceInterfaceA proxyA;

    @Autowired
    private ProxyServiceInterfaceB proxyB;

    @Autowired
    private BackendService backendService;

    @Autowired
    private MyConsumingService myConsumingService;

    @Test
    public void testProxyCreated() {
        assertThat(this.proxyA).isNotNull();
    }

    @Test
    public void testProxyIsProxy() {
        assertThat(Proxy.isProxyClass(this.proxyA.getClass())).isTrue();
        assertThat(Proxy.isProxyClass(this.proxyB.getClass())).isTrue();
    }

    @Test
    public void testDoSomething() {
        assertThat(this.proxyA.doSomething()).isEqualTo(BackendService.RESULT);
    }

    @Test
    public void testDoSomethingElse() {
        assertThat(this.proxyB.doSomethingElse()).isEqualTo(BackendService.RESULT);
    }

    @Test
    public void testProxyInvocationHandlerImplementation() {
        InvocationHandler invocationHandler = Proxy.getInvocationHandler(this.proxyA);
        assertThat(invocationHandler).isInstanceOf(ProxyInvocationHandlerImplementation.class);

        ProxyInvocationHandlerImplementation proxyInvocationHandlerImplementation = (ProxyInvocationHandlerImplementation) invocationHandler;
        assertThat(proxyInvocationHandlerImplementation.getBackendService()).isNotNull().isEqualTo(this.backendService);
    }

    @Test
    public void testSameInstanceA() {
        assertThat(this.proxyA).isSameAs(this.myConsumingService.getProxyServiceInterfaceA());
    }

    @Test
    public void testSameInstanceB() {
        assertThat(this.proxyB).isSameAs(this.myConsumingService.getProxyServiceInterfaceB());
    }
}
