package de.humanfork.experiment.spring.beandefinition;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class MyConsumingServiceTest {

    @Autowired
    private MyConsumingService myConsumingService;

    @Test
    public void testProxyServiceInterfaceAInjection() {
        assertThat(this.myConsumingService.getProxyServiceInterfaceA()).isNotNull();
    }

    @Test
    public void testProxyServiceInterfaceBInjection() {
        assertThat(this.myConsumingService.getProxyServiceInterfaceB()).isNotNull();
    }
}
