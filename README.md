Experiment: How to create multiple Spring Beans in a factory
============================================================

Problem:
--------

One want to have interfaces (`ProxyServiceInterfaceA`/`B`) that are implemented by Java Dynamic Proxies.
(`ProxyInvocationHandlerImplementation` one instance for each interface. They implements `java.lang.reflect.InvocationHandler`.) 
This interface (instances) should become Spring Beans, so that they can be used for autowiring.
The proxy implementation itself should been have access to Spring Beans (`BackendService`) too. 

So this is a bit like Spring Data JPA Repositories.

```
+---------------------+
| @Service            |
| MyConsumingService  | @Autowired                   +------------------------+
|                     |----------------------------->| ProxyServiceInterfaceA |
|                     |                              +------------------------+
|                     |                                           ^
|                     | @Autowired    +------------------------+  :
|                     |-------------->| ProxyServiceInterfaceB |  :
+---------------------+               +------------------------+  :
                                       ^                          :
         <<implements (dynamic Proxy>> :                          :
                                       :                          :
/------------------------------------------\   /------------------------------------------\
| ProxyInvocationHandlerImplementation : B |   | ProxyInvocationHandlerImplementation : A |
\------------------------------------------/   \------------------------------------------/
                     | "@Autowired"                          | "@Autowired"             
                     |                                       |               
                     |                                       |     +-----------------+
                     |                                       +---->| @Service        |
                     |                                             | BackendService  |
                     +-------------------------------------------->|                 |
                                                                   +-----------------+
```

~~~
### Note: about multiple instances of `ProxyInvocationHandlerImplementation`

The challenge in this example is to have multiple instances of `ProxyInvocationHandlerImplementation`.
But if one do not need different instances, and one is enought then an simple Spring Factory would be enought.

In this experiment I want to explore the way of how to have multiple instances, even if in this experiment there is no explicite need for this requriement.
~~~

Solution:
---------

A `ImportBeanDefinitionRegistrar` (`ProxyBeanRegistrar`) create the `BeanDefinition`s for the two instances of `ProxyInvocationHandlerImplementation`.
The trick is that, in order to create Spring Beans for Java Dynamic Procies that should been have access to other Spring Beans,
one must *not* create the prrocies as `instanceSupplier` of the `BeanDefinition`.
Instead one must have a factory method (`ProxyInvocationHandlerImplementation.newProxyInstance`) and register them as
bean factory in the `BeanDefinition`.
There one can also define references to other beans that are needed by the factory method.

```
public class ProxyInvocationHandlerImplementation implements InvocationHandler {

    private final BackendService backendService;
    
    @Override
    public Object invoke(final Object proxy, final Method method, final Object... args) throws Throwable {
       ...
    }
    
    /**
     * Factory method to create an new Proxy that forward the invocations to an new implementation instance.
     */
    public static <T> T newProxyInstance(ClassLoader classLoader, Class<T> implementingInterface,
            BackendService backendService) {

        return implementingInterface.cast(Proxy.newProxyInstance(classLoader,
                new Class[] { implementingInterface },
                new ProxyInvocationHandlerImplementation(backendService)));
    }

}
```

```
public class ProxyBeanRegistrar implements ImportBeanDefinitionRegistrar, BeanClassLoaderAware {

    private ClassLoader beanClassLoader;

    @Override
    public void registerBeanDefinitions(AnnotationMetadata importingClassMetadata,
            BeanDefinitionRegistry registry,
            BeanNameGenerator importBeanNameGenerator) {
                        
            registry.registerBeanDefinition("A", proxyBean(ProxyServiceInterfaceA.class));
            registry.registerBeanDefinition("B", proxyBean(ProxyServiceInterfaceA.class));
            
    }
    
    
    private <T> AbstractBeanDefinition proxyBean(final Class<T> implementingInterface) {
        return BeanDefinitionBuilder
                .rootBeanDefinition(ProxyInvocationHandlerImplementation.class)
                .addConstructorArgValue(this.beanClassLoader)
                .addConstructorArgValue(implementingInterface)
                .addConstructorArgReference("backendService")  //reference to BackendService
                .setFactoryMethod("newProxyInstance")          //factory method
                .getBeanDefinition();
    }

}
```

~~~
### Note: about static factory method: `ProxyInvocationHandlerImplementation.newProxyInstance`

The static factory method is just one way to implement the factory.
An other would be implementing a factory bean.
The advantage of the static method over the factory bean is that no additional class is needed and everything is nicely packed together.
The diadvantage is that the "autowiring" needs to be done explicite in `BeanDefinition(Builder)` with `add(ConstructorArg/Value)Refernce)" in a not type save way.

The factory bean way, with type save autowiring, would look like this:
```
public class ProxyBeanRegistrar implements ImportBeanDefinitionRegistrar {
    ...

    private <T> AbstractBeanDefinition proxyBean(final Class<T> implementingInterface) {
        return BeanDefinitionBuilder
                .rootBeanDefinition(implementingInterface)
                .setFactoryMethodOnBean("newProxyInstance", ProxyBeanFactory.PROXY_BEAN_FACTORY_NAME)
                .addConstructorArgValue(implementingInterface)
                .getBeanDefinition();
    }
}

@Component(value = ProxyBeanFactory.PROXY_BEAN_FACTORY_NAME)
public class ProxyBeanFactory implements BeanClassLoaderAware {
    
    public static final String PROXY_BEAN_FACTORY_NAME = "de.humanfork.experiment.spring.beandefinition.proxy.ProxyBeanFactory";

    @Setter
    private ClassLoader beanClassLoader;

    @Autowired
    private BackendService backendService;

    public <T> T newProxyInstance(final Class<T> implementingInterface) {
        return ProxyInvocationHandlerImplementation
                .newProxyInstance(this.beanClassLoader, implementingInterface, this.backendService);
    }
)
```
~~~

BeanFactoryAware
----------------

`org.springframework.beans.factory.BeanFactoryAware`
Interface to be implemented by beans that wish to be aware of their owning `BeanFactory`.


ImportBeanDefinitionRegistrar
-----------------------------

`org.springframework.context.annotation.ImportBeanDefinitionRegistrarImportBeanDefinitionRegistrar`
Interface to be implemented by types that register additional bean definitions when processing @{@link Configuration} classes.


See also `org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor`
> Extension to the standard `BeanFactoryPostProcessor` SPI, allowing for the registration of further bean definitions _before_ regular `BeanFactoryPostProcessor` detection kicks in.
> In particular, `BeanDefinitionRegistryPostProcessor` may register further bean definitions which in turn define `BeanFactoryPostProcessor` instances.

One difference is the way they are registered, and that `BeanFactoryAware` has access to the "Import" annotation.


BeanDefinition
--------------

A `org.springframework.beans.factory.config.BeanDefinition` (interface) describes a bean instance.

> This is just a minimal interface: The main intention is to allow a `BeanFactoryPostProcessor` to introspect and modify property values and other bean metadata.

(If the described bean class has `@Autowire` annotations, then they become injected by default.)

Bean definitions can be created just plain by

```
GenericBeanDefinition beanDefinition = new GenericBeanDefinition();
beanDefinition.setBeanClass(PojoA.class);
```

or with the help of an Builder `BeanDefinitionBuilder`:
```
BeanDefinition beanDefinition = BeanDefinitionBuilder
                .genericBeanDefinition(PojoA.class)
                .getBeanDefinition();
```

The plain way has some more fine tuning options, but the builder has some sofisticated features and show how to implement them in the plain way:

* `BeanDefinitionBuilder.addPropertyReference(String name, String beanName)` : `this.beanDefinition.getPropertyValues().add(name, new RuntimeBeanReference(beanName))`
* `BeanDefinitionBuilder.addAutowiredProperty(String name)` : `this.beanDefinition.getPropertyValues().add(name, AutowiredPropertyMarker.INSTANCE);`

### AbstractBeanDefinition

`AbstractBeanDefinition` base implementation of `BeanDefinition`.

#### Properties

A `AbstractBeanDefinition` can have properties and constructor parameters.
Both can be a just a normal instance or it can be a reference (to a not yet resolved) other beans (`RuntimeBeanReference`):

Example for property values and references:
```
GenericBeanDefinition beanDefinition = new GenericBeanDefinition();
beanDefinition.setBeanClass(PojoA.class);

MutablePropertyValues propertyValues = new MutablePropertyValues();
propertyValues.add("someNormalInstance", new MyNormalInstance());
propertyValues.add("backendService", new RuntimeBeanReference("backendService"));
beanDefinition.setPropertyValues(propertyValues);
```

with builder
```
BeanDefinition beanDefinition = BeanDefinitionBuilder
                .genericBeanDefinition(PojoA.class)
                .addPropertyValue("someNormalInstance", new MyNormalInstance())
                .addPropertyReference("backendService", "backendService")
                .getBeanDefinition();
```

#### AbstractBeanDefinition.instanceSupplier

It has the attribute `instanceSupplier`
> Specify a callback for creating an instance of the bean, as an alternative to a declaratively specified factory method.
>
> If such a callback is set, it will override any other constructor or factory method metadata.
> However, bean property population and potential annotation-driven injection will still apply as usual.

But this does not help in this experiment, because the `instanceSupplier` has no access to other bean definitions.


### AbstractBeanDefinition.factoryMethodName and factoryBeanName

The attributes `factoryMethodName` and `factoryBeanName` can be used to name 

* a static factory method on the same class, or
* a factory method an a other bean 

In both cases, the `AbstractBeanDefinition.indexedArgumentValues` are the parameter used to invoke the factory method.

Lombok
------
This project use [Project Lombok](https://projectlombok.org/) therefore you need to have Lombok installed

### Lombok - Eclipse - Overlapping text edits bug
There is one problem with Eclipse: The cleanup setting with "Code Style / Use parentheses in expressions".
If it is enabled, then Eclipse cleanup will fail for `@Data` annotated classes:
>An unexpected exception occured while performing the refactoring.
>
>Overlappping text edits
https://bugs.eclipse.org/bugs/show_bug.cgi?id=535536

References
----------

* [Spring - Dynamically register](beans https://www.logicbig.com/tutorials/spring-framework/spring-core/bean-definition.html)